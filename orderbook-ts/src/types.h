//
// Created by Rohit Chandel on 12/12/19.
//

#ifndef ORDERBOOK_TS_TYPES_H
#define ORDERBOOK_TS_TYPES_H

#include <vector>
#include <map>
#include <unordered_map>

#include "deque"

using namespace std;

namespace OB {
    enum class ActionType {
        Add,
        Delete,
        Modify,
        Undefined
    };

    typedef double Price;
    typedef unsigned long Quantity;
    typedef unsigned long OrderId;
    // Side Ask=1, Bid=0
    typedef unsigned int Side;
    const Side ASK = 1;
    const Side BID = 0;
    typedef unsigned long Timestamp;
    typedef struct {
        Timestamp timestamp;
        Price price;
        Quantity qty;
        OrderId id;
        Side side;
        ActionType action;
    } OrderMsg;

    // We may be be interested in saving trades for building strategy
    typedef struct {
        Timestamp timestamp;
        Price price;
        Quantity qty;
    } Trade;

    typedef unordered_map<OrderId, OrderMsg> OrderMap;

    typedef vector<Trade> TradeHistory;

    //deque to maintain FIFO on orders at same price level
    typedef deque<OrderId> OrderList;
    // map to maintain order list at price level
    typedef map<Price, OrderList> PriceOrderMap;
}
#endif //ORDERBOOK_TS_TYPES_H
