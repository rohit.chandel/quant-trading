//
// Created by Rohit Chandel on 12/12/19.
//
#include "parser.h"

using namespace std;

namespace OB {
    bool Parser::parseMsg(string &line, OrderMsg& ord, char delimiter) {
        vector<string> row;
        parseRow(line, row, delimiter);
        if (row.size() != 6) {
            return false;
        }
        ord.timestamp = stol(row[0], nullptr);

        if (row[1] == "b") {
            ord.side = OB::BID;
        }
        else if (row[1] == "a") {
            ord.side = OB::ASK;
        }
        else {
            return false;
        }

        if (row[2]=="a") {
            ord.action = ActionType::Add;
        }
        else if (row[2]=="m") {
            ord.action = ActionType::Modify;
        }
        else if (row[2]=="d") {
            ord.action = ActionType::Delete;
        }
        else {
            return false;
        }
        ord.id = stol(row[3], nullptr);

        ord.price = stod(row[4], nullptr);

        ord.qty = stol(row[5], nullptr);
        return true;

    }

}