#include <iostream>
#include<vector>
#include <fstream>
#include <chrono>

#include "ob_engine.h"

using namespace std;

void read_csv(string & path, string & date, OB::OBEngine & engine) {
    std::ifstream raw_file(path.c_str(), std::ios::in);
    // path where parsed csv is saved
    int counter = 0;
    auto start = chrono::steady_clock::now();

    if (raw_file.is_open()) {
        std::string line;
        std::getline(raw_file, line); // skip the header

        while(getline(raw_file,line)) {
            engine.handleFeed(line);
            counter++;
        }
        raw_file.close();
        cout << "Number of msgs processed: "
             << counter << endl;
    }
    else {
        std::cerr << "Unable to open file at path: " << path << std::endl;
    }
    auto end = chrono::steady_clock::now();
    auto time = chrono::duration_cast<chrono::microseconds>(end - start).count();

    if (counter !=0) {
        cout << "Total time: "
             << time
             << " Per msg: " << (time / counter)  << " microseconds" << endl;
    }

}

int main(int argc, char** argv) {
    if (argc < 4) {
        std::cerr << "Usage: " << argv[0] << "DATE(YYYYMMDD) INPUT_PATH OUT_PATH" << std::endl;
        return 0;
    }
    string date(argv[1]);
    string inputDir(argv[2]);
    string  outDir(argv[3]);
    stringstream ss;
    ss << inputDir << "/res_" << date << ".csv" ;
    string filename =  ss.str();

    OB::OBEngine  engine;

    read_csv(filename, date, engine);

    // save the order time series
    engine.stash(outDir, date);
    return 0;
}
