//
// Created by Rohit Chandel on 12/12/19.
//
#include <algorithm>
#include <sstream>
#include <fstream>

#include "ob_engine.h"

using namespace std;
namespace OB {

    void OBEngine::handleFeed(std::string &line) {
        OrderMsg ord;
        bool result = m_parser.parseMsg(line, ord);
        if (!result) {
            std::cerr << "Malformed msg: " << line << std::endl;
        }
        auto existingOrderIter = m_openOrders.find(ord.id);
        OrderMsg &oldOrder = existingOrderIter->second;

        if (existingOrderIter == m_openOrders.end()) {
            // we have a new order
            if (ord.action == ActionType::Add) {
                add(ord);
            } else {
                cerr << "New order msg is not of add action type." << endl;
                return;
            }
        } else {
            if (ord.action == ActionType::Modify) {
                modify(ord);
            } else if (ord.action == ActionType::Delete) {
                remove(ord);
            } else {
                cerr << "Old order msg id is not of delete or modify action type." << endl;
                return;
            }
        }
        saveSnapshot(ord);
    }

    void OBEngine::matchOrdersAtSamePriceLevel(OB::OrderList &orderList, OB::OrderMsg &ord) {
        // execute all orders at current price level which can be matched with this order
        while (!orderList.empty() && ord.qty != 0) {
            // take the first order in the queue to execute to maintain time priority
            OrderId id = orderList.front();
            auto ordIter = m_openOrders.find(id);
            if (ordIter == m_openOrders.end()) {
                orderList.pop_front();
            } else {
                OrderMsg &matchingOrd = ordIter->second;
                Quantity diff = matchingOrd.qty - ord.qty;
                if (diff <= 0) {
                    // current order partially matched
                    matchingOrd.qty = 0;
                    ord.qty = -diff;
                    m_openOrders.erase(ordIter);
                    orderList.pop_front();
                } else {
                    // full match with current order
                    ord.qty = 0;
                    matchingOrd.qty = diff;
                }
            }
        }
    }

    void OBEngine::handleTrade(OB::OrderMsg &ord) {
        if (m_askSideBook.empty() || m_bidSideBook.empty() || ord.qty == 0) {
            return;
        }
        Quantity initialQty = ord.qty;

        if (ord.side == OB::BID) {
            // iterate from lowest ask
            auto bookIter = m_askSideBook.begin();
            if (ord.price >= bookIter->first) {
                cout << "New buy order msg with " << ord.id << " can be matched with lowest ask." << endl;
            }
            while (bookIter != m_askSideBook.end() && ord.price >= bookIter->first && ord.qty > 0) {

                OrderList &orderList = bookIter->second;
                matchOrdersAtSamePriceLevel(orderList, ord);
                if (orderList.empty()) {
                    m_askSideBook.erase(bookIter++);
                }
                bookIter++;
            }
        } else {
            // iterate from highest bid
            auto bookIter = m_bidSideBook.rbegin();
            if (ord.price <= bookIter->first) {
                cout << "New sell order msg with " << ord.id << " can be matched with highest bid." << endl;
            }
            while ( bookIter != m_bidSideBook.rend() && ord.price <= bookIter->first && ord.qty > 0) {
                OrderList &orderList = bookIter->second;
                matchOrdersAtSamePriceLevel(orderList, ord);
                if (orderList.empty()) {
                    m_bidSideBook.erase(bookIter->first);
                }
                bookIter++;
            }
        }
        if (ord.qty == 0) {
            cout << "Trade fully executed at price: " << ord.price << " with qty: " << initialQty << endl;
        }
        else if (ord.qty < initialQty) {
            cout << "Order partially executed at price: " << ord.price << " with qty: " << initialQty - ord.qty << endl;
        }
    }

    void OBEngine::add(OB::OrderMsg &ord) {
        // check if order causes trade to happen
        handleTrade(ord);
        if (ord.qty == 0) {
            // this will happen in case of market order
            return;
        }

        PriceOrderMap &priceOrdMap = ord.side == OB::BID ? m_bidSideBook : m_askSideBook;

        // get the order list if price already exists in the book
        auto existingPriceList = priceOrdMap.find(ord.price);
        if (existingPriceList == priceOrdMap.end()) {
            OrderList ordList;
            ordList.push_back(ord.id);
            priceOrdMap.insert({ord.price, ordList});
        } else {
            // price level exists
            OrderList &ordList = existingPriceList->second;
            ordList.push_back(ord.id);
        }
        m_openOrders.insert({ord.id, ord});
    }

    void OBEngine::remove(const OB::OrderMsg &ord) {
        auto existingOrderIter = m_openOrders.find(ord.id);
        OrderMsg & existing = existingOrderIter->second;
        if (ord.price != existing.price || ord.qty != existing.qty || ord.side != existing.side) {
            cerr << "Delete order id: " << ord.id << " does not match with open orders." << endl;
            return;
        }
        PriceOrderMap &priceOrdMap = ord.side == OB::BID ? m_bidSideBook : m_askSideBook;
        auto existingPriceList = priceOrdMap.find(ord.price);
        if (existingPriceList != priceOrdMap.end()) {
            OrderList &ordList = existingPriceList->second;
            auto itr = std::find(ordList.begin(), ordList.end(), ord.id);
            if (itr != ordList.end())
                ordList.erase(itr);
            if (ordList.empty()) {
                priceOrdMap.erase(existingPriceList);
            }
            // remove from open orders also
            m_openOrders.erase(existingOrderIter);
        } else {
            cerr << "Trying to remove order id which is unavailable in book" << endl;
            return;
        }
    }

    void OBEngine::modify(OB::OrderMsg &ord) {
        auto oldOrderIter = m_openOrders.find(ord.id);
        OrderMsg & oldOrder = oldOrderIter->second;
        remove(oldOrder);
        add(ord);
    }

    Quantity OBEngine::computeOrderQuantity(OB::OrderList &ord) {
        Quantity totalQty = 0;
        for (auto it = ord.begin(); it != ord.end(); it++) {
            auto ordIter = m_openOrders.find(*it);
            if (ordIter != m_openOrders.end()) {
                totalQty = totalQty + ordIter->second.qty;
            }
        }
        return totalQty;
    }


    void OBEngine::saveSnapshot(OrderMsg &update) {
        // saving snapshot in  expected parsed form
        int levels = 5;
        std::stringbuf buffer;
        std::ostream os(&buffer);
        // update timestamp and price
        os << update.timestamp << ',' << update.price;
        int counter = 0;
        // bid side
        Price lastPrice = 0;
        for (auto it = m_bidSideBook.rbegin(); it != m_bidSideBook.rend() && counter < levels; it++) {
            os << ',' << it->first << ',' << computeOrderQuantity(it->second);
            counter++;
            lastPrice = it->first;
        }
        // fill remaining levels
        while (counter < levels) {
            os << ',' << lastPrice << ',' << 0;
            counter++;
        }
        // ask side
        lastPrice = 0;
        counter = 0;
        for (auto it = m_askSideBook.begin(); it != m_askSideBook.end() && counter < levels; it++) {
            os << ',' << it->first << ',' << computeOrderQuantity(it->second);
            counter++;
            lastPrice = it->first;
        }
        // fill remaining levels
        while (counter < levels) {
            os << ',' << lastPrice << ',' << 0;
            counter++;
        }
        string snap = buffer.str();
        m_orderTS.push_back(snap);
    }

    void OBEngine::stash(string &path, string& date) {
        ofstream  outputFile;
        stringstream ss;
        ss << path << "/ORDER_BOOK." << date << ".csv" ;
        string filename =  ss.str();
        outputFile.open(filename, std::ofstream::out | std::ofstream::trunc);
        if (outputFile.is_open()) {
            //write header
            outputFile << "timestamp"<< ',' << "price";
            for(int i=0; i < 5; i++) {
                outputFile << ',' << "bp" << i <<',' << "bq" << i;
            }
            for(int i=0; i < 5; i++) {
                outputFile << ',' << "ap" << i <<',' << "aq" << i;
            }
            outputFile << "\n";
            //write rows
            for (auto &it : m_orderTS) {
                outputFile << it << "\n";
            }
        }
        else {
            cerr << "Unable to open: " << filename << " for writing parsed data." << endl;
        }
        cout << "Number of order book rows created: "
             << m_orderTS.size() << endl;
        outputFile.close();
    }
}