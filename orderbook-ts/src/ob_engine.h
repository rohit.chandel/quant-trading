//
// Created by Rohit Chandel on 12/12/19.
//

#ifndef ORDERBOOK_TS_OB_ENGINE_H
#define ORDERBOOK_TS_OB_ENGINE_H

#include <iostream>

#include "parser.h"

namespace OB {

    class OBEngine {
    private:
        Parser m_parser;

        // time series of order book snapshots
        vector<string> m_orderTS;

        // ask side book
        PriceOrderMap m_askSideBook;

        // bid side book
        PriceOrderMap m_bidSideBook;
        // map to keep track of all orders since start of the trading day
        OrderMap m_openOrders;

        // helper function of handleTrade to match limit orders at same price level with an incoming market order
        void matchOrdersAtSamePriceLevel(OB::OrderList & ordList, OB::OrderMsg& ord);

        // sum the quantity from order list
        Quantity computeOrderQuantity(OrderList& ord);
    public:
        void handleFeed(string &line);

        /*
         * This function is used to handle trade. If a buy order update is more than the lowest sell price
         * then order matching engine should make a trade. Similarily for the other side.
         * But msgs from our data never crossed ask or bid prices which makes sense because all the orders
         * are limit orders.
         * */
        void handleTrade(OB::OrderMsg & ord);

        void add(OrderMsg &ord);

        void remove(const OrderMsg &ord);

        void modify(OrderMsg &ord);

        //saves the latest order book snapshot
        void saveSnapshot(OB::OrderMsg& update);

        // stash csv file of order book snapshots time series
        void stash(string &path, string& date);

    };

}
#endif //ORDERBOOK_TS_OB_ENGINE_H
