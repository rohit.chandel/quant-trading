//
// Created by Rohit Chandel on 12/12/19.
//

#ifndef ORDERBOOK_TS_PARSER_H
#define ORDERBOOK_TS_PARSER_H

#include <vector>
#include <string>
#include <iostream>
#include <sstream>

#include "types.h"

using namespace std;

namespace OB {
    class Parser {
    private:
        void parseRow(string &tok, vector<string> &row, char delimiter=',') {
            string word;
            stringstream s(tok);
            while (getline(s, word, delimiter)) {
                row.push_back(word);
            }
        }

    public:
        bool parseMsg(string &line, OrderMsg &ord, char delimiter=',');
        // stores the  timeseries in the given path
    };
}

#endif //ORDERBOOK_TS_PARSER_H
