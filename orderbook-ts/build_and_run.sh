#!/usr/bin/env bash
# exit when any command fails
set -e
PROJECT_DIR=$(pwd)
IN_DIR="${PROJECT_DIR}/../raw"
OUT_DIR="${PROJECT_DIR}/../parsed"
LOG_DIR="${PROJECT_DIR}/logs"


function cleanup() {
  echo "Cleaning existing build folder..."
  rm -rf "${PROJECT_DIR}/build"
}

function build() {
  echo "running build.."
  mkdir -p build && cd build
  cmake ../
  make
  echo "build successfully completed."
}

function run() {
  dates=(
    20190610
    20190611
    20190612
    20190613
    20190614
  )
  for date in "${dates[@]}"; do
    echo "Parsing raw data for date ${date}..."
    logfile="${LOG_DIR}/${date}.log"
    rm -f "${logfile}"
    ./orderbook_ts "$date" "${IN_DIR}" "${OUT_DIR}" > "${logfile}" 2>&1
    echo "${date} parsed successfully!"
  done
}

function main() {
  cleanup
  build
  run
}

main