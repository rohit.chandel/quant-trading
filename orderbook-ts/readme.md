## orderbook-ts

Reads order msgs from a csv file and  builds/updates the aggregate order book for each order update,
and outputs a time series of order book states with 5
levels on each side.

### Build
```bash
mkdir -p build && cd build
cmake ../
make
```

### Build and run 
This generates the parsed time series for given dates(see the dates in
build_and_run.sh script)
```bash
./build_and_run.sh
```

### Parse new files
If you want to generate time series for new file, add that into raw folder.
Now to run, make sure file is named as res_YYYYMMDD.csv. Output is generated
in DIRPATH_OF_OUTPUT_FILE as ORDER_BOOK_YYYYMMDD.csv.

Use the binary with following arguments:
```bash
./orderbook_ts DATE(YYYYMMDD) path/to/rawdir path/to/outputdir
```

### Logs
Logs generated in logs folder for every day. Logs contain the information
of basic stats and errors in raw data.

### Data Structure and Algorithm
Each side of order book is created as map of price to queue of orders.
A queue is maintained at each price level to match orders who arrived. Although
we dont need it with this data, as we never get market order msgs. But
order book has been designed to handle market orders which will cause partial 
or full order execution. We also update the order book state accordingly.

Additionally an open orders map is used to keep track of all open orders.

```c++
typedef deque<OrderId> OrderList;
typedef map<Price, OrderList> PriceOrderMap;
typedef unordered_map<OrderId, OrderMsg> OrderMap;
class OBEngine {
    private:
        PriceOrderMap m_askSideBook;
        PriceOrderMap m_bidSideBook;
        OrderMap m_openOrders;
}
```
### Time Complexity

```$xslt
N: number of price level on a side.
M: number of orders at a price level
Add Order : As map is implemented using red black tree. Add order takes logN
Delete: logN + M
Modify: First delete old order and add new. (logN + M)
```
