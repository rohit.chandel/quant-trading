##Genesis

Genesis is name given to modelling and signal generation part of the problem.
 
Genesis has two python packages:

lib: library to aid and speed up “genesis” of new trading signal.

pod: This is the place where ideas come to life. 

This package uses the tools from lib to create new signals.

### Dev setup
1. Create isolated environment to avoid compatibility issues  with global python packages.
Create a new conda environment with given setup. To install conda follow this
[link](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)

```bash
#create env
cd genesis
conda env create -f environment.yml
source activate qt-env
```


### Run instruction

```
# make sure you are in top level genesis folder.
# do not cd into internal genesis

python -m genesis

```

You should see logs similar to ./logs/genesis.log file



### Design

1. lib package contains the reasearch library code
2. pod package contains the signal building modules 
