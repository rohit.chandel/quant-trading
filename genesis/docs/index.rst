.. genesis documentation master file, created by
   sphinx-quickstart on Mon Dec 16 02:13:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to genesis's documentation!
===================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   genesis


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
