genesis.lib package
===================

Submodules
----------

genesis.lib.backtester module
-----------------------------

.. automodule:: genesis.lib.backtester
   :members:
   :undoc-members:
   :show-inheritance:

genesis.lib.error module
------------------------

.. automodule:: genesis.lib.error
   :members:
   :undoc-members:
   :show-inheritance:

genesis.lib.retriever module
----------------------------

.. automodule:: genesis.lib.retriever
   :members:
   :undoc-members:
   :show-inheritance:

genesis.lib.ts module
---------------------

.. automodule:: genesis.lib.ts
   :members:
   :undoc-members:
   :show-inheritance:

genesis.lib.tsa module
----------------------

.. automodule:: genesis.lib.tsa
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: genesis.lib
   :members:
   :undoc-members:
   :show-inheritance:
