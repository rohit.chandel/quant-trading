"""
This is the main program to generate our signal between 
start timestamp and end timestamp for midprice change.
This script also show in sample and out of sample performance 
of lasso regression with cross validation model.
This also back tests the trading strategy based on this signal. 
"""

# core modules
import logging
import pandas as pd
import os

# genesis modules
import genesis
import genesis.lib.backtester as backtester
import genesis.lib.retriever as retriever
import genesis.lib.ts as ts


def start():
    logger = logging.getLogger('GENESIS.ROOT')
    signame = 'genesis.pod.ob.midpricechange'
    cfg = genesis.config
    backtest_source = cfg[signame]['backtest_source']
    # run backtests for all signals
    for signame in list(cfg.keys()):
        start_time = pd.Timestamp('20190610', tz='UTC')
        end_time = pd.Timestamp('20190610', tz='UTC')
        # get features and target of signal
        logger.info('Building model on first 2 days data %s'
                     % signame)
        dfx, dfy = retriever.get_features_and_target(
                signame,
                start_time,
                end_time
                )
        logger.info('Features and target generated for %s'
                     % signame)
        logger.info('Fitting Lasso with cross validation...')
        logger.info('This will take a while...')

        reg = ts.regress(dfy, dfx,
                              model='LassoCV',
                              random_state=0,
                              max_iter=50000,
                              cv=5,
                              normalize=True,
                              fit_intercept=True)
        logger.info('Fitting Done.')
        dfx  = dfx.fillna(0)
        dfy  = dfy.fillna(0)
        score = reg.score(dfx, dfy.values.ravel())
        logger.info('IN SAMPLE SCORE: %f' % score)
        logger.info('Top 20 features selected by lasso')
        dfbeta = pd.DataFrame({
                'features': dfx.columns,
                'weights': reg.coef_
                })
        dfbeta = dfbeta.iloc[(-dfbeta['weights'].abs()).argsort()]
        # take top 20
        dfbeta = dfbeta[:20]
        ax = dfbeta.plot.bar(x='features',
                             figsize=(15,8),
                             y='weights',
                             rot=60)
        ax.set_title('Top features by abs weights.')
        fig = ax.get_figure()

        fig.savefig(os.path.join(backtest_source, 'feature_wts.png'))
        
        logger.info('TESTING ON OUT OF SAMPLE  DATA')
        
        start_time = pd.Timestamp('20190612', tz='UTC')
        end_time = pd.Timestamp('20190612', tz='UTC')
        
        dfxout, dfyout = retriever.get_features_and_target(
                signame,
                start_time,
                end_time
                )
        
        dfxout  = dfxout.fillna(0)
        dfyout  = dfyout.fillna(0)
        score = reg.score(dfxout, dfyout.values.ravel())
        logger.info('OUT OF SAMPLE SCORE: %d' % score)
        logger.info('This shows market condition changes frequentlyr'
                    'in HFT. We need to use small rolling subsample for fitting'% score)

        logger.info('RUNNING BACTESTING FOR COMBINED SIGNAL %s'
                    'FROM OUR ROLLING REGRESSION MODEL'
                     % signame)
        start_time = pd.Timestamp('20190610', tz='UTC')
        end_time = pd.Timestamp('20190611', tz='UTC')
        # running tests see backtests dir for output
        backtester.run(signame, start_time, end_time)
        logger.info('BACTESTING FINISHED FOR SIGNAL'
                    'SEE genesis/backtest DIR FOR RESULTS')
        
if __name__ == '__main__':
    start()
    