"""
Signal to predict mid price change.

Key points:
    1. Mid price change response: Mid price is average price of best ask and best bid. 
    Change in mid price is good proxy for knowing in what direction the market will 
    be moving and also a good proxy of expected returns in high frequency trading.
    Since Order book is highest resolution possible of price movement. Trying to
    predict mid price change for every event may not be good response as order events
    come at very fast speed and mid price changes might be non tradable. Signal generated
    from such model can be unusable in real life. We should resample our order book
    time series to frequency we want to trade on. This can be determined by latency of trading
    platform. For example: If our our strategies are of seconds horizon,
    we should resample time series to 1s.

Author: Rohit Kumar
"""
# core modules
import os
import logging
import numpy as np
import pandas as pd

# genesis modules
import genesis.lib.ts as ts
import genesis


# internal function
def ___compute_features_and_target___(df, lags=1):
    """
    helper function computes features  and log return
    of mid prices target to orderbook time series
    Returns:
        pd.Dataframe: df of features
        pd.Dataframe: df of target
    """
    
    orig_columns = df.columns
    # features data frame
    dfout = df.copy()
    
    # log price change (price_order_t+1 - price_order_t)
    # This feature is not that interesting  in my opinion for our response.
    # But lets create it and we can worry about this in feature extraction.
    
    dfout['log_ret_price'] = np.log(df['price']) - np.log(df['price'].shift(1))
    
    levels = 5
    
    aq_cols = [ ('aq%d' % i) for i in range(levels)]
        
    bq_cols = [ ('bq%d' % i) for i in range(levels)]
    
#    ap_cols = [ ('ap%d' % i) for i in range(levels)]
#        
#    bp_cols = [ ('bp%d' % i) for i in range(levels)]
    

    # we are taking log transform for variance stablization
    # one of the condition of regression is homoskadasticity
    # log transformation also reduces skewness and makes patterns more
    # interpretable
    
    # Log transformation helps in achieving symmetrical distribution
    # because the log transformation stretches apart data with
    # small values and shrinks together data with large values.
    
    for i in range(0, levels):
        # first feature is log return on ask side
        
        # on ask side
        dfout['log_ret_ap%d' % i] = np.log(
                dfout['ap%d' % i]) - np.log(dfout['ap%d' % i].shift(1))
        # change in volume
        dfout['log_change_aq%d' % i] = np.log(
                dfout['aq%d' % i]) - np.log(dfout['aq%d' % i].shift(1))
        # on bid side
        dfout['log_ret_bp%d' % i] = np.log(
                dfout['bp%d' % i]) - np.log(dfout['bp%d' % i].shift(1))
        # change in volume
        dfout['log_change_bq%d' % i] = np.log(
                dfout['bq%d' % i]) - np.log(dfout['bq%d' % i].shift(1))
        
        
        # bid ask spreads
        dfout['log_ap%d_bp%d' % (i,i)] = np.log(
                dfout['ap%d' % i]) - np.log(dfout['bp%d' % i].shift(1))
        
        # volume spreads
        dfout['log_aq%d_bq%d' % (i,i)] = np.log(
                dfout['aq%d' % i]) - np.log(dfout['bq%d' % i].shift(1))
        
        
        
        if i!=0:
            # diff of other level for best bid and ask level
            # this feature is for the shape of order book
            dfout['log_ap%d_ap0' % i] = np.log(
                dfout['ap%d' % i]) - np.log(dfout['ap0'])
            dfout['log_bp0_bp%d' % i] = np.log(
                dfout['bp0']) - np.log(dfout['bp%d' % i])
            dfout['log_ap%d_ap0' % i] = np.log(
                dfout['aq%d' % i]) - np.log(dfout['aq0'])
            dfout['log_bq0_bq%d' % i] = np.log(
                dfout['bq0']) - np.log(dfout['bq%d' % i])
            
    # total supply feature can be sum of all sell orders
    
    dfout['log_tot_aq'] = np.log(dfout[aq_cols].sum(axis=1))
    
    # total demand feature can be sum of all buy orders
    dfout['log_tot_bq'] = np.log(dfout[bq_cols].sum(axis=1))
    
    # average ask side volume but this can be highly
    # correlated with log_tot_aq, watch out for multi colleanearity
    dfout['log_avg_bq'] = np.log(dfout[bq_cols].mean(axis=1))
    
    # average bid side volume but this can be highly
    # correlated with log_tot_vq, watch out for multi colleanearity
    
    dfout['log_avg_bq'] = np.log(dfout[bq_cols].mean(axis=1))
    
    # order book dispersion
    # This feature shows how clustered 
    # limit orders are in the LOB.
    # buy side dispersion
    
    # disp = np.log(dfout[apcols].diff(axis=1).abs().sum(axis=1))
    # dfout['log_adisp'] = disp/dfout['log_adisp']
    
    # mid price
    dfout['mp'] = (dfout['ap0'] + dfout['aq0'])/2
    target = 'log_ret_mp'
    # this is our target variable
    dfout[target] = np.log(
            dfout['mp']) - np.log(dfout['mp'].shift(1))
    
    # using lags variable of above features
    
    for col in orig_columns:
        dfout[col] = np.log(dfout[col])

    # handle inf values in columns
    for col in dfout.columns:
        dfout[col] = dfout[col].replace((np.inf, -np.inf), (0, 0))
    
    # identified irrelevant features for out target
    # after doing exploratory data analysis.
    # drop these features
    irrelevant_features = ['mp', 'price', ]
    dfout = dfout.drop(irrelevant_features, axis=1)
    
    features = dfout.columns.drop([target])
    
    # add the lagged features also
    #key value pair of lagged feature to value of lagged feature
    lagged_feat = {'%s_t_%d' % (f, i) : dfout[f].shift(i)
        for f in features for i in range(1,lags + 1)}
    # unpack above dictionary for assign function
    # here basically we are adding lagged features to original
    # dataframe. refer to pd.asssign docs
    dfout = dfout.assign(**lagged_feat)
    features = dfout.columns.drop([target])

    

    # shifting features by one period since we create the signal
    # at the end of period with lag 1. it means features at the the end
    # of current period will predict the target of text period
    dfout[features] = dfout[features].shift(1)
        
    # after all the feature engineering ,
    # we can resample here our df to frequency which our strategy trades on
    # I am not doing that for this exercise,
    # im assuming our platform can trade on the order message frequency.
    # i am still resampling data to unique timestamp frequency
    # since signals based on same timestamp wont be tradable
    # we keep the latest order book snapshot if we have multiple updates
    # on same time stamp
    # this signal can be now used trade strategy with horizon
    # of few microseconds.
    dfout = dfout.loc[~dfout.index.duplicated(keep='last')]
    
    return dfout[features], dfout[[target]]
 

def get_features_and_target(signame, start_time , end_time):
    """
    public function to expose features and target between
    start_time and end_time.
    This is used for backtesting.
    Returns:
        pd.Dataframe: df of features
        pd.Dataframe: df of target
    """
    
    logging.getLogger(signame).info('Generating features for signal %s from %s to %s'
                     %(signame,
                       start_time.strftime('%Y-%m%-%d %H:%M:%S'),
                       end_time.strftime('%Y-%m%-%d %H:%M:%S')
                       ))
    cfg = genesis.config[signame]
    source = cfg['raw_source']
    dfraw = ts.read_periodic(source,
                       start_time,
                       end_time,
                       logger_name=signame)
    dffeat, dftarget = ___compute_features_and_target___(dfraw, lags=1)
    return dffeat, dftarget
    

def get_signal(signame, start_time , end_time):
    """
    function to get the time series of combined signal
    from fitting lasso regression on order book features
    Args:
        signame(str): name of the signal
        start_timestamp(pd.Timestamp): starting timestamp for which signal
        to generate
        end_timestamp(pd.Timestamp): ending timestamp for which signal
        to generate
    Returns:
        pd.DataFrame: time series of combined signal
    """
    logging.getLogger(signame).info('Generating signal %s from %s to %s'
                     %(signame,
                       start_time.strftime('%Y-%m-%d %H:%M:%S'),
                       end_time.strftime('%Y-%m-%d %H:%M:%S')
                       ))
    
    
    logging.getLogger(signame).info(
            'loading stashed signal instead of regenrating')
    cfg = genesis.config[signame]
    stashed_dir = cfg['backtest_source']    
    # loading stashed signal as building will take time
    # uncomment for generating again
    stashed = True
    if stashed:
         dfsig = pd.read_pickle(os.path.join(stashed_dir, 'mpchg_sig.pkl'))
         return dfsig
    lookback_window = cfg['lookback_window']
    dfx, dfy = get_features_and_target(signame, start_time , end_time)
    
    # clip outtliers beyond 3 sigma 
    # as these features are not exactly normally distributed
    # since them have skewness and fatter tails
    
    dfx = ts.rolling_tclip(dfx, threshold=4,
                           window=lookback_window,
                           min_periods=lookback_window)

    dfy = ts.rolling_tclip(dfy, threshold=4,
                           window=lookback_window,
                           min_periods=lookback_window)
    
    # taking look back window of approximately 10000 periods to
    # fit the regression model to get the weights for features.
    # 10000 is reasonable in hft as markets change quickly
    # look back window can be one of the hyperparameter
    # but I am avoiding that for this exercise.
    
    ## instead of using Lasso im using lasso with cross
    ## validation to find optimal penalty alpha multiplier also
    
    # ols is faster so using that for regenerating
    # uncomment this if you want lasso sig
    
#    dfcoefs, dftscores = ts.rolling_regress(dfy, dfx,
#                                                  step=1,
#                                                  model='LassoCV',
#                                                  window=lookback_window,
#                                                  random_state=0,
#                                                  max_iter=10000,
#                                                  cv=5,
#                                                  normalize=True,
#                                                  fit_intercept=True)
    
    dfcoefs, dftscores = ts.rolling_regress(dfy, dfx,
                                              step=1,
                                              model='OLS',
                                              window=lookback_window,
                                              fit_intercept=True)
    
    
    logging.getLogger(signame).info(
            'Combining %s signal...' % (signame))
    # combine signal
    # time series dot product 
    # of features * weights
    dfsignal = ts.dot(dfx, dfcoefs)
    # we wont use signal till look back window starts
    # as values are nan before lookback_window. so lets drop those
    # signal rows
    dfsignal = pd.DataFrame(dfsignal[lookback_window:], columns=[signame])
    return dfsignal
    
    
if __name__ == '__main__':
    start_time = pd.Timestamp('20190610', tz='UTC')
    end_time = pd.Timestamp('20190610', tz='UTC')
    signame = 'genesis.pod.ob.midpricechange'
    dfsignal = get_signal(signame, start_time, end_time)
    
        
        
    