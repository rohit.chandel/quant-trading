"""
Frequencty used operations on timeseries dataframe. Time series is defined
as df widh index as pd.Timestamp and  name "TIMESTAMP" 

Author: Rohit Kumar
"""
# core modules
import logging
import numpy as np
import pandas as pd
# lasso with cross validation
from sklearn.linear_model import LassoCV
from sklearn.linear_model import LinearRegression

# genesis modules

from genesis.lib.error import GenesisError

# label of the index of timeseries index. This is made public so that consumer 
# can use it while renameing index.
TIMESTAMP_NAME = 'TIMESTAMP'

def read_periodic(path, start_time, end_time,
                  timestamp_col='timestamp',
                  timestamp_unit='microseconds',
                  logger_name=None, **kwargs):
    """
    Read raw timeseries csv between start and end date.
    Adds UTC TIMESTAMP as index.
    Args:
        name (str): name of 
        path (str): path of the raw file with with %Y%m%d in name
        start_time (pd.Timestamp): start date
        end_time (pd.Timestamp): start date
        timestamp_col(str): column name of timstamp in intraday file
        timestamp_unit(str): string, {‘ns’, ‘us’, ‘ms’, ‘s’, ‘m’, ‘h’, ‘D’,
        'microseconds', 'nanoseconds'},
        optional
        logger_name(str): label used for logger name
        kwargs(dict): kay=value args passed to pandas read_csv
    Returns:
        pd.Datafram: timeseries of data
    """
    dates = pd.date_range(start_time, end_time, freq='B')
    logger = None
    if logger_name:
        logger = logging.getLogger(logger_name)
    dfcombined = []
    for date in dates:
        file = date.strftime(path)
        if logger:
            logger.info('Reading file %s' %(file))
        dfcurr = pd.read_csv(file, **kwargs)
        if timestamp_col not in dfcurr.columns:
            logger.info('Skipping file %s as no %s column.'
                        %(file, timestamp_col))
        dfcurr[timestamp_col] = pd.to_timedelta(dfcurr[timestamp_col],
              unit=timestamp_unit)
        dfcurr[TIMESTAMP_NAME] =  date + dfcurr[timestamp_col]
        if dfcurr is not None and not dfcurr.empty:
            dfcombined.append(dfcurr)
    if len(dfcombined) == 0:
        return None
    if logger:
        logger.info('Reading done. Combining Files...')
    dfcombined = pd.concat(dfcombined)
    dfcombined = dfcombined.drop(timestamp_col, axis=1)
    if logger:
            logger.info('Combined.')
    dfcombined = dfcombined.set_index(TIMESTAMP_NAME)
    return dfcombined
    

def rolling_mean(df,
                window=10000,
                min_periods=None,
                ):
    """
    Computes rolling mean
    Args:
        df(pd.DataFrame): ts dataframe
        min_period(int): min periods to compute mean
    Returns:
        pd.DataFrame: mean dataframe
    """
    if not is_ts(df):
        raise GenesisError('df should be valid time series dataframe')
    df = df.rolling(window, min_periods=min_periods).mean()
    return df
   
    
def rolling_std(df,
                window=10000,
                min_periods=None,
                ):
    """
    Computes rolling std
    Args:
        df(pd.DataFrame): ts dataframe
        window(int): rolling window size
        min_period(int): min periods to compute std
    Returns:
        pd.DataFrame: std dataframe
    """
    if not is_ts(df):
        raise GenesisError('df should be valid time series dataframe')
    df = df.rolling(window, min_periods=min_periods).std()
    return df


def rolling_tscore(df,
           window=10000,
           min_periods=None
           ):
    """
    Standardize the time series to be
    with mean 0 and std 1 in rolling window
    Args:
        df(pd.DataFrame): ts dataframe to standardise
        window(int): rolling window size
        min_period(int): min periods to compute mean and std
    Returns:
        pd.DataFrame: trimmed dataframe
    """
    if not is_ts(df):
        raise GenesisError('df should be valid time series dataframe')
    df = df.copy()
    dfmean = rolling_mean(df,
                          window=window,
                          min_periods=min_periods)
    dfstd = rolling_std(df,
                        window=window,
                        min_periods=min_periods) 
    # filtering to get min periods functionality
    # borrowed from pandas
    df = df[dfmean.notnull()]
    df = (df - dfmean).div(dfstd)
    return df


def rolling_tclip(df,
                  threshold=3,
                  window=10000,
                  min_periods=None
                  ):
    """
    Removes the outlier in ts in rolling window
    above given threshold and clip to the threshold value
    Args:
        df(pd.DataFrame): ts dataframe to trim
        threshold(int): outlier removal threshold
        window(int): rolling window size
        min_period(int): min periods to compute mean and std
    Returns:
        pd.DataFrame: trimmed dataframe
    """
    if not is_ts(df):
        raise GenesisError('df should be valid time series dataframe')
    df = df.copy()
    dfmean = rolling_mean(df,
                          window=window,
                          min_periods=min_periods)
    dfstd = rolling_std(df,
                        window=window,
                        min_periods=min_periods)
    # filtering to get min periods functionality
    # borrowed from pandas
    df = df[dfmean.notnull()]
    pos_threshold = dfmean + threshold*dfstd
    neg_threshold = dfmean - threshold*dfstd
    pos_outlier_mask = (df > pos_threshold)
    neg_outlier_mask  = (df < neg_threshold)
    # clip values
    df[pos_outlier_mask] = pos_threshold
    df[neg_outlier_mask] = neg_threshold
    return df

    
def is_ts(df):
    """
    checks if df is a valid time series
    key checks - DATE index
    Args:
        df(pd.DataFrame): dataframe to check
    Returns:
        boolean
    Raises:
        GenesisError
    """
    if TIMESTAMP_NAME != df.index.name:
        return False
    if not isinstance(df.index, pd.DatetimeIndex):
        return False
    return True

# internal function

def rolling_regress(dfy, dfx, step=1, window=1000,
                    model='OLS',
                    min_period=1000,
                    fill_value=None,
                    **kwargs):
    """
    Rolling lasso regression or ols regression
    with given window and step size
    Loss uses cross validation to find the optimal alpha penalty
    
    Args:
        dfy(pd.DataFrame): time series of y variable
        dfx(pd.DataFrame): time series of x variables
        step(pd.DataFrame): step size
        model('str'): model to fit(LassoCV | OLS)
        window(int): window size
        min_period(int): minium periods required for regression
        fill_value(int): fill nans with this value
        fit_intercept(bool): fit intercept in regression,
        kwargs: all the key value parameters accepted by sklearn
        LassoCV
    Returns:
        pd.DataFrame: df of beta coeffcients
        pd.DataFrame: df of scores        
    """
    if model == 'OLS':
        model_class = LinearRegression
    elif model == 'LassoCV':
        model_class = LassoCV
    else:
        raise GenesisError('Unknow model. Pass OLS or LassoCV')

    if not is_ts(dfy) or not is_ts(dfx):
        raise GenesisError('dfx and dfy should valid time series dataframes')
    if fill_value is not None: 
        dfy = dfy.fillna(fill_value)
        dfx = dfx.fillna(fill_value)
    else:
        logging.getLogger('LassoCV').warning(
                "Dropping features rows with missing values.")
        dfy = dfy.dropna()
        dfx = dfx.dropna()
    dfbetalist = []
    dfscoreslist = []
    for i in range(window, dfx.shape[0], step):
        if i%100 == 0:
            logging.getLogger(model).info('Running Rolling %s for %d row' % (
                    model, i-window)) 
        dfxcurr = dfx.iloc[i-window: i]
        dfycurr = dfy.iloc[i-window: i]
        curr_reg = model_class(**kwargs).fit(dfxcurr, dfycurr.values.ravel())
        dfcurrbeta = pd.DataFrame(np.array([curr_reg.coef_]),
                                  columns=dfx.columns)
        score = curr_reg.score(dfxcurr, dfycurr.values.ravel())
        dfcurrbeta[TIMESTAMP_NAME] = dfx.index[i]
        dfscore = pd.DataFrame({'score': [score]})
        dfscore[TIMESTAMP_NAME] = dfx.index[i]
        dfscoreslist.append(dfscore)
        dfbetalist.append(dfcurrbeta)
    
    if len(dfbetalist)==0 :
        return None, None
    dfbeta = pd.concat(dfbetalist)
    dfscores = pd.concat(dfscoreslist)
    dfbeta = dfbeta.set_index(TIMESTAMP_NAME)
    dfscores = dfscores.set_index(TIMESTAMP_NAME)
    return dfbeta, dfscores


def regress(dfy, dfx, model='OLS', 
            fill_value=0,
            **kwargs):
    """
    lasso regression or ols regression
    Args:
        dfy(pd.DataFrame): time series of y variable
        dfx(pd.DataFrame): time series of x variables
    Returns:
        np.array: list of coeffs
        float: score       
    """
    if model == 'OLS':
        model_class = LinearRegression
    elif model == 'LassoCV':
        model_class = LassoCV
    else:
        raise GenesisError('Unknow model. Pass OLS or LassoCV')

    if not is_ts(dfy) or not is_ts(dfx):
        raise GenesisError('dfx and dfy should valid time series dataframes')
    if fill_value is not None: 
        dfy = dfy.fillna(fill_value)
        dfx = dfx.fillna(fill_value)
    else:
        logging.getLogger('LassoCV').warning(
                "Dropping features rows with missing values.")
        dfy = dfy.dropna()
        dfx = dfx.dropna()
    reg = model_class(**kwargs).fit(dfx, dfy.values.ravel())
    return reg


def dot(dfa, dfb):
    """
    row wise dot product of two time series
    """
    if not is_ts(dfa) or not is_ts(dfb):
        raise GenesisError('dfa and dfv should valid time series dataframes')
    return dfa.mul(dfb).sum(axis=1) 
    


if __name__ == '__main__':
    #for testing
    start_time = pd.Timestamp('20190610', tz='UTC')
    end_time = pd.Timestamp('20190614', tz='UTC')

        
        
    