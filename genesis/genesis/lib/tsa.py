"""
Time series analysis module to basically genrate 
the testing signal results.
This module will also used by back tester for
doing time series analaysis of signals, holdings and returns.

Author: Rohit Kumar
"""
# core modules 
import os
import numpy as np
# genesis module
import genesis.lib.ts as ts


class TSA:
    """
    TSA class to test signal results
    """
    def  __init__(self, label=None,
                plotter='matplotlib',
                plotdir=None):
        """
        Constructor
        Args:
            label(str): label to be used for plotting
            plotter(str): matplotlib or None if plots are not required
            plotDir(str): path to output result dir
        """
        self.label = label
        self.plotter = plotter
        self.plotdir = plotdir
        
    
    def get_holdings(self, signame, dfsig):
        """
        simple holding optimizer to get basic holding.
        holding +1 when sig positive othewise -1.
        """
        df = dfsig.copy()
        df['holding'] = df[signame]
        df.loc[df[signame]>0, 'holding']= 1
        df.loc[df[signame]<0, 'holding']= -1
        df = df.drop(signame, axis=1)
        return df
        
    def get_returns(self, signame, dfret, dfh):
        """
        returns from portfolio
        """
        dfh = dfh.rename(columns={dfh.columns[0]: signame})
        dfret = dfret.rename(columns={dfret.columns[0]: signame})
        return dfh.mul(dfret)
    
    def cum_ret(self, dfret):
        """
        get the cumulative returns
        as our returns our log returns, so cum sum is equivalent
        """
        dfcumret = dfret.cumsum(axis=0)
        ax = dfcumret.plot()
        ax.set_title('Cumulutive returns of mid price signal')
        fig = ax.get_figure()
        fig.savefig(os.path.join(self.plotdir, 'cum_ret.png'))        
        return dfcumret
        

    def rolling_sharpe(self, dfret, window=5):
        """
        computes the daily rolling sharpe(not precise computation).
        we comupute cumulative 1 min return and use that to compute
        daily sharpe.
        here window is the number of minutes.      
        """
        minr_ret = dfret.resample('1T').sum().fillna(0)
        dfmean_ret_rolling = ts.rolling_mean(minr_ret, window=window,
                                 min_periods=window)
        dfstd_ret_rolling = ts.rolling_std(minr_ret, window=window,
                                 min_periods=window)
        # assuming benchmark to be 0 at so high frequency
        dfrolling_sharpe = dfmean_ret_rolling.div(dfstd_ret_rolling)
        # transforming it to daily sharpe
        dfrolling_sharpe = dfrolling_sharpe*np.sqrt(8*60)
        ax = dfrolling_sharpe.plot()  # s is an instance of Series
        ax.set_title('Rolling sharpe of mid price signal')
        fig = ax.get_figure()
        fig.savefig(os.path.join(self.plotdir, 'rolling_sharpe.png'))
        return dfrolling_sharpe
        
        
        
      