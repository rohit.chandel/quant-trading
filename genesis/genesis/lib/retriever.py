"""
module to fetch signal data dynamically just using name of
signal

Author: Rohit Kumar
"""
# core modules
import importlib

# import genesis modules
import genesis
from genesis.lib.error import GenesisError

def get_features_and_target(signame, start_time, end_time):
    """
    function to access signal features and target
    between start_time and end_time
    Args:
        signame(str): name of the signal
        start_time(pd.Timestamp): starting timestamp for which signal
        to read
        end_time(pd.Timestamp): ending timestamp for which signal
        to read
    Returns:
        pd.DataFrame: read signal df 
    Raises:
        GenesisError: error when signal does not exist.
    """
    signame = 'genesis.pod.ob.midpricechange'
    cfg = genesis.config
    if signame not in cfg:
        raise GenesisError("Signal %s does not exist in config file."
                     % signame)
    try:
        sig_module = importlib.import_module(signame)
    except ModuleNotFoundError:
        raise GenesisError("Signal %s does not exist." % signame)
    if sig_module:
        return sig_module.get_features_and_target(signame,
                                                  start_time,
                                                  end_time)
    return None


def get_signal(signame, start_time, end_time):
    """
    function to access signal data
    between start_time and end_time
    Args:
        signame(str): name of the signal
        start_time(pd.Timestamp): starting timestamp for which signal
        to read
        end_time(pd.Timestamp): ending timestamp for which signal
        to read
    Returns:
        pd.DataFrame: read signal df 
    Raises:
        GenesisError: error when signal does not exist.
    """
    signame = 'genesis.pod.ob.midpricechange'
    try:
        sig_module = importlib.import_module(signame)
    except ModuleNotFoundError:
        raise GenesisError("Signal %s does not exist." % signame)
    if sig_module:
        return sig_module.get_signal(signame,
                                      start_time,
                                      end_time)
    return None
