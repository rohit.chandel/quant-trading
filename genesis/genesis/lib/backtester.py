"""
Backtesting module

Author: Rohit Kumar

"""
# core modules
import logging

# genesis modules
import genesis
import genesis.lib.tsa as tsa
import genesis.lib.retriever as retriever




def run(signame, start_time, end_time):
    # get signal time series
    cfg = genesis.config[signame]
    plotdir = cfg['backtest_source']
    logger = logging.getLogger(signame)
    # path to dump results
    logger.info( 'READING ALREADY PICKLED SIGNAL FOR FASTER TESTING...')
    dfsignal = retriever.get_signal(signame, 
                                   start_time=start_time,
                                   end_time=end_time)
    # get signal features and target
    dffeat, dftarget = retriever.get_features_and_target(
            signame, 
            start_time=start_time,
            end_time=end_time)
    
    # as we created signal for  small sample
    # lets reindex target to our signal index
    dftarget = dftarget.reindex(dfsignal.index)
    
    # perform analysis
    signal_analytics = tsa.TSA(label=signame, plotdir=plotdir)

    # holding df of single stock 
    # hold +1 when positive signal and -1 when negative signal
    dfh = signal_analytics.get_holdings(signame, dfsignal)
    # returns of the holdings of 
    dfsigret = signal_analytics.get_returns(signame, dfh, dftarget)
    # at the end of period cumulative return is
    dfcumret = signal_analytics.cum_ret(dfsigret)
    cum_ret = dfcumret.iloc[-1].values[0]*100
    logging.getLogger(signame).info(
            'Cumlative return in 2000 periods out of sample is %f percent'
            % (cum_ret))
    
#    dfsharpe = signal_analytics.rolling_sharpe(dfsigret,
#                                               window=30)
#    meansharpe = dfsharpe.mean()
#    logging.getLogger(signame).info(
#            'Mean daily sharpe of signal is %f'
#            % (meansharpe))

    
    