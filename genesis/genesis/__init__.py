import json
import os
import logging

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

with open(os.path.join(ROOT_DIR, 'config.json')) as config_file:
    config = json.load(config_file)

# update all the source paths in config to append ROOT_DIR
# this is to change relative paths to abs path
for key in list(config.keys()):
    curr_cfg = config[key]
    for subkey in list(curr_cfg.keys()):
        if '_source' in subkey:
            curr_cfg[subkey] =os.path.join(ROOT_DIR,
                    curr_cfg[subkey]
                  )

# LOG_DIR = os.path.join(ROOT_DIR, '../logs')

# set global  basic settings of logger
logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.NOTSET,
    datefmt='%Y-%m-%d %H:%M:%S')
